def vowel_swapper(string):
    # ==============
    # Your code here
    a=string.replace("a","4")
    A=a.replace("A","4")
    e=A.replace("e","3")
    E=e.replace("E","3")
    i=E.replace("i","!")
    I=i.replace("I","!")
    o=I.replace("o","ooo")
    O=o.replace("O","OOO")
    u=O.replace("u","|_|")
    U=u.replace("U","|_|")
    return U




    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
